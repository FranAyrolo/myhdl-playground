import sys
import os
sys.path
sys.path.append('../sim/')
from riscv_alu import riscv_alu
from riscv_alu import ALUOp
from riscv_alu import ALUPortIO
import random
from myhdl import *


def testbench(aluIO, clk, rst):

    rst_flag = Signal(bool(0))
    
    @always(delay(1))
    def clkgen():
        clk.next = not clk

    @always(delay(10))
    def rstgen():
        if (rst_flag == 0):
            rst.next = 1
            rst_flag.next = 1
        else:
            rst.next = 0    

    @always(clk.posedge)
    def stimulus():
        aluIO.alu_a_i.next = random.randrange(0, 2**32)
        aluIO.alu_b_i.next = random.randrange(0, 2**32)
        aluIO.alu_op_i.next = random.randrange(0,16)

        a = aluIO.alu_a_i
        b = aluIO.alu_b_i
        sel = aluIO.alu_op_i
        shamt = aluIO.alu_b_i[5:0]
        
        if sel == ALUOp.ALU_ADD:
            assert aluIO.alu_p_o == modbv(a + b)[32:], "Error ADD"
        elif sel == ALUOp.ALU_SHIFTL:
            assert aluIO.alu_p_o == modbv(a << shamt)[32:], "Error SLL"
        elif sel == ALUOp.ALU_XOR:
            assert aluIO.alu_p_o == a ^ b, "Error XOR"
        elif sel == ALUOp.ALU_SHIFTR:
            assert aluIO.alu_p_o == a >> shamt, "Error SRL"
        elif sel == ALUOp.ALU_OR:
            assert aluIO.alu_p_o == a | b, "Error OR"
        elif sel == ALUOp.ALU_AND:
            assert aluIO.alu_p_o == a & b, "Error AND"
        elif sel == ALUOp.ALU_SUB:
            assert aluIO.alu_p_o == modbv(a - b)[32:], "Error SUB"
        elif sel == ALUOp.ALU_SHIFTR_ARITH:
            assert aluIO.alu_p_o == modbv(a.signed() >> shamt)[32:], "Error SRA"
        elif sel == ALUOp.ALU_LESS_THAN_SIGNED:
            assert aluIO.alu_p_o == modbv(a.signed() < b.signed())[32:], "Error SL"
        elif sel == ALUOp.ALU_LESS_THAN:
            assert aluIO.alu_p_o == modbv(abs(a) < b)[32:], "Error SLTU"
        elif sel == ALUOp.ALU_NONE:
            assert aluIO.alu_p_o == a, "Error UNDEFINED OP"
    return clkgen, rstgen, stimulus

def cosim(aluIO, clk, rst):
    print "COSIM"
    cmd = 'iverilog -o alu_riscv -c ../rtl/filelist.f'
    os.system(cmd)
    return Cosimulation("vvp -m ./myhdl.vpi alu_riscv", input_a=aluIO.alu_a_i,input_b=aluIO.alu_b_i,selector=aluIO.alu_op_i,clk = clk, rst = rst,output_alu=aluIO.alu_p_o)

if __name__ == "__main__":
    
    clk = Signal(bool(0))
    rst = Signal(bool(0))
    aluIO = ALUPortIO()

    tb = testbench(aluIO, clk, rst)
    
    cosim_inst = cosim(aluIO,clk,rst)
    
    sim = Simulation(tb,cosim_inst)
    sim.run(1000)
    #testbench = _testbench()
    #testbench.config_sim(trace=True)
    #testbench.run_sim(2000)
